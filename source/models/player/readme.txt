This package contains the Renamon player model for Vore Tournament.
Model created by Shadowsquirrel and Slime_Demon_Kishin: http://www.furaffinity.net/view/3682911 & http://www.furaffinity.net/view/7706382
Ported by MirceaKitsune.
Renamon belongs to Toei Animation and is part of the Digimon series.
