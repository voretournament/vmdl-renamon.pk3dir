textures/player/renamon_eyes
{
	dpreflectcube cubemaps/default/sky
 	{
		map textures/player/renamon_eyes.tga
		rgbgen lightingDiffuse
	}
}

textures/player/renamon_internal_mouth
{
	dpreflectcube cubemaps/default/sky
	deformVertexes wave 25 sin 0 0.2 0 2
	{
		map textures/player/renamon_mouth.tga
		rgbgen lightingDiffuse
	}
}

textures/player/renamon_internal_stomach
{
	dpreflectcube cubemaps/default/sky
	deformVertexes wave 100 sin 0 2 0 3
	{
		map textures/player/renamon_stomach.tga
		rgbgen lightingDiffuse
	}
}
